# IPL Data Project 1
---

### To Create JSON files from Given CSV 
```
npm run csvToJSON
```

### To Run IPL Program which will Redirect all outputs to their corresponding JSON Files 
```
npm run IPL
```
---

**Default Problems Branch**
* master

**Refactored Problems Branch**
* refactor

**Additional 5 Problems Branch**
* refactor

---

**Node Package used to convert from .csv to .json**
* [csvtojson](https://www.npmjs.com/package/csvtojson)