
// 1. Number of matches played per year for all the years in IPL.

function matchesPerYear(matchesList,deliveriesList){

    if(matchesList==undefined){
        return {}
    }

    let matchesInEveryYearObj={}

    for(let i=0;i<matchesList.length;i++){
       // console.log(matchesList[i])
        if(matchesInEveryYearObj[matchesList[i].season]==undefined){
            matchesInEveryYearObj[matchesList[i].season]=1;
        }else{
            matchesInEveryYearObj[matchesList[i].season]++;
        }
    }
    return matchesInEveryYearObj;
}




// 2. Number of matches won per team per year in IPL.
function matchesWonPerTeamPerYear(matchesList,deliveriesList){

    if(matchesList==undefined){
        return {}
    }

    let matchesWonPerTeamPerYearObj={}
    
    for(let i=0;i<matchesList.length;i++){
        if(matchesList[i].winner==""||matchesList[i].winner=="\n"||matchesList[i].winner==undefined){
            continue;
        }

        const winner=matchesList[i].winner
        const season=matchesList[i].season

//        console.log(`${season}  ${team}`)

        if(matchesWonPerTeamPerYearObj[winner]==undefined){
            matchesWonPerTeamPerYearObj[winner]={}
        }

        if(matchesWonPerTeamPerYearObj[winner][season]==undefined){
           matchesWonPerTeamPerYearObj[winner][season]=0;
        }  
        
        matchesWonPerTeamPerYearObj[winner][season]++;
        
    }
    return matchesWonPerTeamPerYearObj;
}



// 3. Extra runs conceded per team in the year 2016
function extraRunsConcededPerYearIn2016(matchesList,deliveriesList){
   
    if(deliveriesList==undefined){
        return {}
    }

    let extraRunsConcededPerYearObj={}

    for(let i=0;i<deliveriesList.length;i++){

        const extraRuns=deliveriesList[i].extra_runs

        if(extraRuns>0){

            const bowlingTeam=deliveriesList[i].bowling_team

            //console.log(`${battingTeam}  ${extraRuns}`)

            if(extraRunsConcededPerYearObj[bowlingTeam]==undefined){
                extraRunsConcededPerYearObj[bowlingTeam]=0;
            }
            extraRunsConcededPerYearObj[bowlingTeam]++;
        }
    }

    return extraRunsConcededPerYearObj
}









// 4. Top 10 economical bowlers in the year 2015

function topTenEconomicalBowlersIn2015(matchesList, deliveriesList) {
  if (matchesList == undefined || deliveriesList == undefined) {
    return [];
  }
  
  let matchIdof2015 = [];

  for (let i = 0; i < matchesList.length; i++) {
    if (matchesList[i].season == "2015") {
      if (matchIdof2015.indexOf(matchesList[i].id) == -1) {
        matchIdof2015.push(matchesList[i].id);
      }
    }
  }

  if (matchIdof2015.length<0) {
    return [];
  }

  let bowlersTotalRunsConceded = {};

  for (let i = 0; i < deliveriesList.length; i++) {
   
    const currentMatchid = deliveriesList[i].match_id;
   const bowlerName = deliveriesList[i].bowler;

    if (matchIdof2015.indexOf(currentMatchid)> 0) {
      if (bowlersTotalRunsConceded[bowlerName] == undefined) {
        bowlersTotalRunsConceded[bowlerName] = {
          totalRuns: 0,
          bowls: 0,
        };
      }

      bowlersTotalRunsConceded[bowlerName].totalRuns += Number(deliveriesList[i].total_runs);
     
      if(!(deliveriesList[i].wide_runs>0 ||deliveriesList[i].noball_runs>0)){
        bowlersTotalRunsConceded[bowlerName].bowls++;
      }
    }
  }

  // Calculating economy by using totalRuns and bowls
  for (let bowler in bowlersTotalRunsConceded) {
    const totalRuns = bowlersTotalRunsConceded[bowler].totalRuns;
    bowlersTotalRunsConceded[bowler].economy =((6*totalRuns) / bowlersTotalRunsConceded[bowler].bowls).toFixed(2);
  }
  
   const topTenEconomialBowlers = [];
   for(let bowler in bowlersTotalRunsConceded){
     topTenEconomialBowlers.push({
       bowler:bowler,
       economy:bowlersTotalRunsConceded[bowler].economy
     })
   }

   // sort in ascending order of economy
  topTenEconomialBowlers.sort((a,b)=>{
    return a.economy-b.economy
  })

  return topTenEconomialBowlers.slice(0,10);
}



module.exports={ 
    matchesPerYear,
    matchesWonPerTeamPerYear,
    extraRunsConcededPerYearIn2016,
    topTenEconomicalBowlersIn2015
}



