const fs = require('fs')
const csv=require("csvtojson");
const path=require('path')

const deliveriesAddress = `${__dirname}/../../data/deliveries.csv`; 
const matchesAddress=`${__dirname}/../../data/matches.csv`


function renameFilePath(filePath){
    return filePath.replace('.csv','.json')
}

const outputDeliveriesAddress=renameFilePath(deliveriesAddress); 
const outputMatchesAddress=renameFilePath(matchesAddress); 


console.log('Getting Data!')

//console.log(`curr path ${__dirname}`)

function csvTOJSON(csvFilePath,jsonFilePath){
    csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{
    //console.log(jsonObj);
    writeToJSON(jsonFilePath,jsonObj)
})
}

function writeToJSON(jsonFilePath,data){
  
    fs.writeFile(jsonFilePath,JSON.stringify(data),function(err,data){
        if(err){
            console.log('some error occurred while writing to json.')
        }else{
            console.log('Data written Successfully')
        }
    })
}


csvTOJSON(deliveriesAddress,outputDeliveriesAddress)
csvTOJSON(matchesAddress,outputMatchesAddress)