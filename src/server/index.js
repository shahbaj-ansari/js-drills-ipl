const fs = require("fs");
const path=require('path')

const deliveriesData=require(path.join(__dirname,'../../data/deliveries.json'))
const matchesData=require(path.join(__dirname,'../../data/matches.json'))


//console.log(matchesList)

const {
  matchesPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerYearIn2016,
  topTenEconomicalBowlersIn2015,
} = require("./ipl");


function executeProblem(matchesList,deliveriesList,problemName){
	const result=problemName(matchesList,deliveriesList)
  const outputPath=path.join(__dirname,`../../public/output/${problemName.name}.json`);
	
	if(result!=undefined && outputPath!=undefined){
		//console.log(result)
		fs.writeFileSync(outputPath,JSON.stringify(result,null,' '));
	}else{
    console.log('some error occurred while writing.')
  }
}

executeProblem(matchesData,deliveriesData,matchesPerYear)

executeProblem(matchesData,deliveriesData,matchesWonPerTeamPerYear)

executeProblem(matchesData,deliveriesData,extraRunsConcededPerYearIn2016)

executeProblem(matchesData,deliveriesData,topTenEconomicalBowlersIn2015)

